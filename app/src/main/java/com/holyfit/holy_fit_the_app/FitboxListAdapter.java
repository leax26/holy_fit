package com.holyfit.holy_fit_the_app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.holyfit.holy_fit_the_app.model.Fitbox;

import java.util.ArrayList;
import java.util.List;

public class FitboxListAdapter extends RecyclerView.Adapter<FitboxListAdapter.FitboxViewHolder> {

    Context context;
    List<Fitbox> fitboxes;

    public FitboxListAdapter(Context context, ArrayList<Fitbox> fitboxes) {
        this.context = context;
        this.fitboxes = fitboxes;
    }

    @NonNull
    @Override
    public FitboxViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.fitbox_card, parent, false);
        return new FitboxViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FitboxViewHolder holder, int position) {
        holder.title.setText(fitboxes.get(position).getTitle());
        holder.description.setText(fitboxes.get(position).getDescription());
        holder.price.setText(fitboxes.get(position).getPrice());
        holder.image.setImageResource(R.drawable.fitbox);
        holder.card.setTag(fitboxes.get(position).getId());
    }

    @Override
    public int getItemCount() {
        if (fitboxes != null) {
            return fitboxes.size();
        } else return 0;
    }


    public class FitboxViewHolder extends RecyclerView.ViewHolder {

        CardView card;
        TextView title, description, price;
        ImageView image;

        public FitboxViewHolder(@NonNull View itemView) {
            super(itemView);
            card = itemView.findViewById(R.id.fitbox_cardView);
            title = itemView.findViewById(R.id.fitbox_title);
            description = itemView.findViewById(R.id.fitbox_description);
            price = itemView.findViewById(R.id.fitbox_price);
            image = itemView.findViewById(R.id.fitbox_img);
        }
    }
}
