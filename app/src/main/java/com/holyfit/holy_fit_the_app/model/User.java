package com.holyfit.holy_fit_the_app.model;


import java.io.Serializable;

public class User implements Serializable {

    private String firstname;
    private String lastname;
    private String email;
    private String phonenumber;
    private Address address;

    public User(){}

    public User(String firstname, String lastname, String email, String phonenumber){
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.phonenumber = phonenumber;
    }

    public User(String firstname, String lastname, String email, String phonenumber, Address address){
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.phonenumber = phonenumber;
        this.address = address;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public Address getAddress() {
        return address;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
