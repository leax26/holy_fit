package com.holyfit.holy_fit_the_app.model;

import java.io.Serializable;
import java.util.List;

public class Fitbox implements Serializable {


    private String id;
    private String title;
    private String description;
    private String extendedDescription;
    private List<String> contents;
    private String price;
//    private int imageUrl;

    public Fitbox() {
    }

    public Fitbox(String title, String description, String price) {
        this.title = title;
        this.description = description;
        this.price = price;
//        this.imageUrl = imageUrl;
    }

    public Fitbox(String title, String description, String extendedDescription, String price, List contents) {
        this.title = title;
        this.description = description;
        this.extendedDescription = extendedDescription;
        this.contents = contents;
        this.price = price;
//        this.imageUrl = imageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public String getDescription() {
        return this.description;
    }

    public String getPrice() {
        return this.price;
    }

//    public int getImageUrl() {
//        return this.imageUrl;
//    }

    public String getExtendedDescription() {
        return this.extendedDescription;
    }

    public List getContents() {
        return this.contents;
    }
}
