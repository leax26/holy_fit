package com.holyfit.holy_fit_the_app.model;

import java.io.Serializable;

public class Address implements Serializable {

    private String streetname;
    private String postalcode;
    private String city;


    public Address(){}

    public Address(String streetname, String postalcode, String city) {
        this.streetname = streetname;
        this.postalcode = postalcode;
        this.city = city;
    }

    public String getStreetname() {
        return this.streetname;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public String getCity() {
        return this.city;
    }

    public void setStreetname(String streetname) {
        this.streetname = streetname;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
