package com.holyfit.holy_fit_the_app;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import com.holyfit.holy_fit_the_app.model.Fitbox;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static com.holyfit.holy_fit_the_app.MainActivity.FITBOX_PREFS_NAME;


public class fit_box extends Fragment {

    public fit_box() {
    }

    private ArrayList<Fitbox> fitboxes = new ArrayList<Fitbox>();
    FitboxListAdapter adapter;
    RecyclerView recyclerView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        this.initFitboxes();
        this.retrieveFitboxes();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//         Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fit_box, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
            this.recyclerView = (RecyclerView) getView().findViewById(R.id.fitbox_recycler_view);
            adapter = new FitboxListAdapter(this.getContext(), fitboxes);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));

            this.checkForOrderCart();
    }

    @Override
    public void onResume() {
        super.onResume();
        this.checkForOrderCart();
    }

    private void checkForOrderCart(){
        SharedPreferences preferences = this.getActivity().getSharedPreferences(FITBOX_PREFS_NAME, MODE_PRIVATE);
        if(preferences.contains("order_cart")){
            FloatingActionButton fab = this.getActivity().findViewById(R.id.shoppingcardButton);
            fab.setVisibility(View.VISIBLE);
        }
    }

    //
    private void retrieveFitboxes() {
        FirebaseDatabase database = FirebaseDatabase.getInstance("https://test-holy-fit-default-rtdb.europe-west1.firebasedatabase.app/");
        DatabaseReference reference = database.getReference("fitbox");

        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Fitbox fitbox = snapshot.getValue(Fitbox.class);
                fitbox.setId(snapshot.getKey());
                fitboxes.add(fitbox);
                adapter.notifyDataSetChanged();

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
//                Fitbox fitbox = snapshot.getValue(Fitbox.class);
                Log.d("test", "test");
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {
                Log.d("test", "test2");
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Log.d("test", "test3");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.w("test", "error", error.toException());
            }
        }
        );
    }
}