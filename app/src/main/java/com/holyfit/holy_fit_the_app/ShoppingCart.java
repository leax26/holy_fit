package com.holyfit.holy_fit_the_app;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;

import java.util.Map;

import static com.holyfit.holy_fit_the_app.MainActivity.FITBOX_PREFS_NAME;

public class ShoppingCart extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceBundle){
        super.onCreate(savedInstanceBundle);
        setContentView(R.layout.activity_shopping_cart);
        TextView overview = findViewById(R.id.overviewShoppingCart);
        String content = this.createString();
        overview.setText(content);
    }

    private String createString(){
        SharedPreferences preferences = getSharedPreferences(FITBOX_PREFS_NAME,MODE_PRIVATE);
        String jsonString = preferences.getString("order_cart","");
        Map<String, Number> orderMap = new Gson().fromJson(jsonString, Map.class);
        String content = "";
        for(Map.Entry<String, Number> entry : orderMap.entrySet()){
            String tempString = "\u2022 fitbox id: "+ entry.getKey() + ", amount: "+entry.getValue()+"\n";
            content += tempString;
        };
        return content;
    }
}
