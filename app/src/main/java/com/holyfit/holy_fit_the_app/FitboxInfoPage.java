package com.holyfit.holy_fit_the_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.holyfit.holy_fit_the_app.model.Fitbox;

import java.util.HashMap;
import java.util.Map;

import static com.holyfit.holy_fit_the_app.MainActivity.FITBOX_PREFS_NAME;

public class FitboxInfoPage extends AppCompatActivity {

    private FirebaseDatabase database;
    private DatabaseReference reference;

    private String fitbox_id;

    private int count = 1;
    private TextView counter;
    private Map orderMap = new HashMap<String, Integer>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fitbox_info_page);

        SharedPreferences prefs = getSharedPreferences(FITBOX_PREFS_NAME, MODE_PRIVATE);
        this.fitbox_id = prefs.getString("fitbox_id", "0");
        database = FirebaseDatabase.getInstance("https://test-holy-fit-default-rtdb.europe-west1.firebasedatabase.app/");
        reference = database.getReference("fitbox").child(this.fitbox_id);
        this.retrieveFitboxData();

    }

    protected void retrieveFitboxData() {
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Fitbox fitbox = snapshot.getValue(Fitbox.class);
                setValues(fitbox);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.w("infoPage", "Database gaf een error terug", error.toException());
            }
        });
    }

    protected void setValues(Fitbox fitbox) {
        TextView title, description, price;

        title = findViewById(R.id.fitbox_infopage_title);
        description = findViewById(R.id.fitbox_infopage_description);
        price = findViewById(R.id.fitbox_infopagge_price);
        counter = findViewById(R.id.fitbox_infopage_counterText);

        title.setText(fitbox.getTitle());
        description.setText(fitbox.getExtendedDescription());
        price.setText("€" + fitbox.getPrice() + " p/s");
        counter.setText("" + this.count);


        setContents(fitbox);
    }

    protected void setContents(Fitbox fitbox) {
        TextView contents = findViewById(R.id.fitbox_infopage_contents);

        String contentString = "";
        for (int index = 0; index < fitbox.getContents().size(); index++) {
            String tempString = "\u2022 " + fitbox.getContents().get(index).toString() + "\n";
            contentString += tempString;
        }
        contents.setText(contentString);

    }

    public void increaseCount(View view) {
        this.count++;
        this.setCounter();
    }

    public void decreaseCount(View view) {
        if (count > 1) {
            this.count--;
            this.setCounter();
        }
    }

    public void setCounter() {
        counter.setText("" + this.count);
    }

    public void addToBasket(View view) {
        SharedPreferences preferences = getSharedPreferences(FITBOX_PREFS_NAME, MODE_PRIVATE);
        if (preferences.contains("order_cart")) {
            this.orderMap = new Gson().fromJson(preferences.getString("order_cart", null), Map.class);
            if (orderMap.containsKey(fitbox_id)) {

                double existingCountDouble = (double) orderMap.get(fitbox_id);
                int existingCount = (int) existingCountDouble;

                existingCount += this.count;
                orderMap.put(fitbox_id, existingCount);
            } else {
                orderMap.put(fitbox_id, this.count);
            }
        } else {
            orderMap.put(fitbox_id, this.count);
        }

        String json = new Gson().toJson(this.orderMap);
        Log.d("jsonString", json);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("order_cart", json);
        editor.commit();
        Log.d("fitbox_id", this.fitbox_id);
        Log.d("fitbox_id", "order amount is: " + count);
        Toast.makeText(this, "Product toegevoegd aan je winkelwagen",Toast.LENGTH_SHORT).show();
        this.finish();
    }

}