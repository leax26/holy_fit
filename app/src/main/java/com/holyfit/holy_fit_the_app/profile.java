package com.holyfit.holy_fit_the_app;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.holyfit.holy_fit_the_app.model.User;

public class profile extends Fragment {


    private FirebaseAuth firebaseAuth;
    private boolean isLoggedIn = false;

    public profile() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user != null) {
            isLoggedIn = true;
            this.fetchUserData(null, user.getUid());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    // after onCreateView
    @Override
    public void onStart() {
        super.onStart();
        if(isLoggedIn){
            this.getActivity().findViewById(R.id.profile_login).setVisibility(View.GONE);
            this.getActivity().findViewById(R.id.profile_overview).setVisibility(View.VISIBLE);
            this.addListenerToFields();
        }
        else {
            this.getActivity().findViewById(R.id.profile_login).setVisibility(View.VISIBLE);
            this.getActivity().findViewById(R.id.profile_overview).setVisibility(View.GONE);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_loggin, container, false);
        return view;
    }

    private void addListenerToFields(){
        EditText firstname, lastname, phonenumber, streetname, postalcode, city;
        Button saveChangesButton = getActivity().findViewById(R.id.saveChangesButton);
        firstname = getActivity().findViewById(R.id.firstname);
        lastname = getActivity().findViewById(R.id.lastname);
        phonenumber = getActivity().findViewById(R.id.phoneNumber);
        streetname = getActivity().findViewById(R.id.streetname);
        postalcode = getActivity().findViewById(R.id.postalcode);
        city = getActivity().findViewById(R.id.city);

        firstname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(saveChangesButton.getVisibility()==View.INVISIBLE || saveChangesButton.getVisibility()==View.GONE){
                    saveChangesButton.setVisibility(View.VISIBLE);
                }
            }
        });
        lastname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(saveChangesButton.getVisibility()==View.INVISIBLE || saveChangesButton.getVisibility()==View.GONE){
                    saveChangesButton.setVisibility(View.VISIBLE);
                }
            }
        });
        phonenumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(saveChangesButton.getVisibility()==View.INVISIBLE || saveChangesButton.getVisibility()==View.GONE){
                    saveChangesButton.setVisibility(View.VISIBLE);
                }
            }
        });
        streetname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(saveChangesButton.getVisibility()==View.INVISIBLE || saveChangesButton.getVisibility()==View.GONE){
                    saveChangesButton.setVisibility(View.VISIBLE);
                }
            }
        });
        postalcode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(saveChangesButton.getVisibility()==View.INVISIBLE || saveChangesButton.getVisibility()==View.GONE){
                    saveChangesButton.setVisibility(View.VISIBLE);
                }
            }
        });
        city.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(saveChangesButton.getVisibility()==View.INVISIBLE || saveChangesButton.getVisibility()==View.GONE){
                    saveChangesButton.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    public void fetchUserData(String email, String id) {
        FirebaseDatabase database = FirebaseDatabase.getInstance("https://test-holy-fit-default-rtdb.europe-west1.firebasedatabase.app/");
        DatabaseReference reference = database.getReference("user").child(id);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User user = snapshot.getValue(User.class);
                if (user != null) {
                    fillViewWithUserData(user);
                    Log.i("user", user.getAddress().getPostalcode());
                } else {
                    Log.i("user", "user is null");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.w("userError", "failed to fetchUser", error.toException());
            }
        });
    }

    private void fillViewWithUserData(User user) {
        EditText firstname, lastname, email, phonenumber, streetname, city, postalcode;
        Activity activity = getActivity();
        firstname = activity.findViewById(R.id.firstname);
        lastname = activity.findViewById(R.id.lastname);
        email = activity.findViewById(R.id.email);
        phonenumber = activity.findViewById(R.id.phoneNumber);
        streetname = activity.findViewById(R.id.streetname);
        city = activity.findViewById(R.id.city);
        postalcode = activity.findViewById(R.id.postalcode);
        if (user.getFirstname() != null) {
            firstname.setText(user.getFirstname());
        }
        if (user.getLastname() != null) {
            lastname.setText(user.getLastname());
        }
        if (user.getEmail() != null) {
            email.setText(user.getEmail());
            email.setActivated(false);
        }
        if (user.getPhonenumber() != null) {
            phonenumber.setText(user.getPhonenumber());
        }
        if (user.getAddress().getStreetname() != null) {
            streetname.setText(user.getAddress().getStreetname());
        }
        if (user.getAddress().getPostalcode() != null) {
            postalcode.setText(user.getAddress().getPostalcode());
        }
        if (user.getAddress().getCity() != null) {
            city.setText(user.getAddress().getCity());
        }
    }


    public void saveChanges(View view){

    }

    public void onSignupClick(View view){}
    public void onLogginClick(View view){}
    public void logoutClicked(View view){}

}