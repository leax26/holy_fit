package com.holyfit.holy_fit_the_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.holyfit.holy_fit_the_app.model.Address;
import com.holyfit.holy_fit_the_app.model.Fitbox;
import com.holyfit.holy_fit_the_app.model.User;

import java.util.ArrayList;
import java.util.List;

import static androidx.navigation.Navigation.findNavController;


public class MainActivity extends AppCompatActivity {

    public static final String FITBOX_PREFS_NAME = "fitbox_prefs";

    private ArrayList<Fitbox> fitboxes = new ArrayList<Fitbox>();
    private List<String> contents = new ArrayList<String>();
    private FirebaseDatabase database;
    private FirebaseAuth firebaseAuth;
    private User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        addContents();
//        initFitboxes();
        initNavController();


    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance("https://test-holy-fit-default-rtdb.europe-west1.firebasedatabase.app/");
//        if(firebaseAuth.getCurrentUser()!= null){
//            fetchUserData(null, firebaseAuth.getCurrentUser().getUid());
//        }
        //        initFitboxDatabase();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPreferences preferences = getSharedPreferences(FITBOX_PREFS_NAME, MODE_PRIVATE);
        preferences.edit().clear().commit();
    }

    private void addContents() {
        this.contents.add("test1");
        this.contents.add("test2");
        this.contents.add("test3");
    }


    private void initNavController() {
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.fit_box, R.id.boot_camp, R.id.videos, R.id.profile)
                .build();

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(bottomNavigationView, navController);
    }

    //
    private void initFitboxes() {
        this.fitboxes.add(new Fitbox("Fitbox klein", "Dit is een kleine doos gevuld met lekkers om je op weg te helpen naar een gezondere toekomst.", "extendedDescription", "12,95", contents));
        this.fitboxes.add(new Fitbox("Fitbox middel", "Dit is een middel grote doos gevuld met lekkers om je op weg te helpen naar een gezondere toekomst.", "extendedDescription", "17,95", contents));
        this.fitboxes.add(new Fitbox("Fitbox groot", "Dit is een grote doos gevuld met lekkers om je op weg te helpen naar een gezondere toekomst.", "extendedDescription", "21,95", contents));
    }

    private void initFitboxDatabase() {
        DatabaseReference reference = database.getReference("fitbox");

        for (int index = 0; index < 3; index++) {
            reference.child("" + index).setValue(this.fitboxes.get(index));
        }
    }

    public void cardClicked(View view) {

        SharedPreferences preferences = getSharedPreferences(FITBOX_PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("fitbox_id", view.findViewById(R.id.fitbox_cardView).getTag().toString());
        editor.commit();
        startActivity(new Intent(this, FitboxInfoPage.class));
    }

    public void fabClicked(View view) {
        Log.d("fab", "buttonIsClicked");
        startActivity(new Intent(this, ShoppingCart.class));
    }

    protected boolean validateSignInForm(String email, String password) {
        boolean valid = true;

        if (email == null || password == null) {
            valid = false;
        }
        if(email.isEmpty() || password.isEmpty()){
            valid = false;
        }
        return valid;
    }

    public void onLogginClick(View view) {
        EditText emailField, passwordField;
        emailField = findViewById(R.id.signin_email);
        passwordField = findViewById(R.id.signin_password);

        String email, password;
        email = emailField.getText().toString();
        password = passwordField.getText().toString();


        if (validateSignInForm(email, password)) {
            firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        FirebaseUser user = firebaseAuth.getCurrentUser();
                        Toast.makeText(getApplicationContext(), "Login van " + email + " is succesvol!", Toast.LENGTH_SHORT).show();
                        Log.i("user", user.getUid());
                        updateUser(email, user.getUid());
                    } else {
                        Log.w("logginError", "firebaseauth went wrong", task.getException());
                        Toast.makeText(getApplicationContext(), "Er is iets fout gegaan bij het inloggen!", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        } else{
            Toast.makeText(this, "Vul alle velden in a.u.b!", Toast.LENGTH_SHORT).show();
        }

    }

    public void onSignupClick(View view) {
        EditText emailField, passwordField;
        emailField = findViewById(R.id.signin_email);
        passwordField = findViewById(R.id.signin_password);

        String email, password;
        email = emailField.getText().toString();
        password = passwordField.getText().toString();

        if (validateSignInForm(email, password)) {
            firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        FirebaseUser user = firebaseAuth.getCurrentUser();
                        Toast.makeText(getApplicationContext(), "Registratie gelukt", Toast.LENGTH_SHORT).show();
                        Log.i("user", user.getEmail());
                        updateUser(email, user.getUid());
                    } else {
                        Log.w("signupError", "failed to register user", task.getException());
                        Toast.makeText(getApplicationContext(), "registreren mislukt", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void updateUser(String email, String id) {
        if (email != null) {
            if (this.user == null) {
                this.user = new User(null, null, email, null);
            }
            fetchUserData(email, id);
        } else {
            this.user = null;
        }

    }

    public void fetchUserData(String email, String id) {
        DatabaseReference reference = database.getReference("user").child(id);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User user = snapshot.getValue(User.class);
                if (user != null) {
                    fillViewWithUserData(user);
                    Log.i("user", user.getAddress().getPostalcode());
                } else {
                    Log.i("user", "user is null");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.w("userError", "failed to fetchUser", error.toException());
            }
        });
    }

    private void fillViewWithUserData(User user) {
        if (findViewById(R.id.profile_overview).getVisibility() == View.GONE) {
            findViewById(R.id.profile_login).setVisibility(View.GONE);
            findViewById(R.id.profile_overview).setVisibility(View.VISIBLE);
        }
        EditText firstname, lastname, email, phonenumber, streetname, city, postalcode;
        firstname = findViewById(R.id.firstname);
        lastname = findViewById(R.id.lastname);
        email = findViewById(R.id.email);
        phonenumber = findViewById(R.id.phoneNumber);
        streetname = findViewById(R.id.streetname);
        city = findViewById(R.id.city);
        postalcode = findViewById(R.id.postalcode);
        if (user.getFirstname() != null) {
            firstname.setText(user.getFirstname());
        }
        if (user.getLastname() != null) {
            lastname.setText(user.getLastname());
        }
        if (user.getEmail() != null) {
            email.setText(user.getEmail());
            email.setActivated(false);
        }
        if (user.getPhonenumber() != null) {
            phonenumber.setText(user.getPhonenumber());
        }
        if (user.getAddress().getStreetname() != null) {
            streetname.setText(user.getAddress().getStreetname());
        }
        if (user.getAddress().getPostalcode() != null) {
            postalcode.setText(user.getAddress().getPostalcode());
        }
        if (user.getAddress().getCity() != null) {
            city.setText(user.getAddress().getCity());
        }
    }

    public void logoutClicked(View view) {
        firebaseAuth.signOut();
        updateUser(null, null);
        findViewById(R.id.profile_overview).setVisibility(View.GONE);
        findViewById(R.id.profile_login).setVisibility(View.VISIBLE);

    }

    public void saveChanges(View view) {
        EditText firstnameField, lastnameField,phonenumberField, streetnameField, postalcodeField, cityField;
        firstnameField = findViewById(R.id.firstname);
        lastnameField = findViewById(R.id.lastname);
        phonenumberField = findViewById(R.id.phoneNumber);
        streetnameField = findViewById(R.id.streetname);
        postalcodeField = findViewById(R.id.postalcode);
        cityField = findViewById(R.id.city);

        String firstname, lastname, phonenumber, streetname, postalcode, city;
        firstname = firstnameField.getText().toString();
        lastname = lastnameField.getText().toString();
        phonenumber = phonenumberField.getText().toString();
        streetname = streetnameField.getText().toString();
        postalcode = postalcodeField.getText().toString();
        city = cityField.getText().toString();

        if(user == null){
            user = new User(null, null, firebaseAuth.getCurrentUser().getEmail(), null);
        }

        user.setFirstname(firstname);
        user.setLastname(lastname);
        user.setPhonenumber(phonenumber);
        user.setAddress(new Address(streetname, postalcode, city));

        DatabaseReference reference = database.getReference("user").child(firebaseAuth.getCurrentUser().getUid());
        reference.setValue(user);


        //update user with all fields
        //update firebase entry

    }

}